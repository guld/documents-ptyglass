# Panama Canal Coin

By implementing a digital token for Panama Canal access, the Canal administrations could save tens of millions of dollars a year in fees, reinvest a large portion of profits, and pay year equal dividends to every Panamanian. Panama Canal Coin (PCC) would allow the spender a discount and other advantages when paying for Canal access.

### Savings

### Timeline

##### Year 1

In the first year, the Panama Canal Authority continues to operate the canal as usual. At the end of the accounting year, if any profits have been made, those profits will be put in the PCC Fund, and 1 PCC coin will be issued for each $1 deposited. These new PCC coins would be evenly distributed to every Panamanian citizen with a cedula, effectively paying them a dividend as owners of the Canal.

For 2016 the Canal profits were $1.2 billion, so we could expect 1.2 billion PCC to be issued at the end of year 1. Since there are 4 million Panamanians, each would receive 300 PCC. Each of these PCC would be backed by $1 which would be held in the PCC Fund.

##### Year 2

In the second year, the Panama Canal Authority would add PCC as an additional option. If a shipping company is able to acquire PCC on the open market, buying from Panamanians, they will be able to make faster, preferred payments for the Canal.

##### Year 3+

Over time the PCC Fund would 

### Key Management

Each Panamanian Citizen would be required to register an official RSA signing key with the Tribunal Electoral. This is the same way that Estonia issues digital ids.

### Costs

By utilizing open source and mature technologies, PCC could be executed at a very low cost, on the order of $40 million to set up the whold country, or roughly $10 per person.

| Item | Cost | Description |
|------|------|-------------|
| openPGP sim cards | $20 million | These special sim cards will turn each Panamanian's phone into his digital cedula, and Canal interface. |
| Software Development | $5 million | Most of the software already exists, but special apps for both the Canal workers and the citizens would be needed. A very conservative estimate for the development of two apps would be $5m. It could be $1m or less for an MVP. |
| 
